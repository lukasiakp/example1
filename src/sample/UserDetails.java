package sample;

public class UserDetails {
	private long Userid;
	private String street;
    private String city;
    
    private User user;
    
    public UserDetails(String street, String city) {
		this.street = street;
		this.city = city;
	}
    
    public UserDetails(){}
    
	public long getUserid() {
		return Userid;
	}
	public void setUserid(long userid) {
		Userid = userid;
	}
	
	
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	
	public String getStreet() {
		return street;
	}
	public void setStreet(String street) {
		this.street = street;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}


    
}
