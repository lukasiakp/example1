package sample;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

public class User {
	   private long id;
	   private String name;
	   private String password; 
	   
	   private Picture picture;
	   private List<Post> posts;
	   
	   
	   private UserDetails userDetails;

	   public User(String name, String password) {
	      this.name = name;
	      this.password = password;
	   }
	   
	   User() {
	   }
	   
	   
	public Picture getPicture() {
		return picture;
	}

	public void setPicture(Picture picture) {
		this.picture = picture;
	}

	public List<Post> getPosts() {
		return posts;
	}

	public void setPosts(List<Post> posts) {
		this.posts = posts;
	}

	public String getName() {
	      return name;
	   }

	   public void setName(String name) {
	      this.name = name;
	   }

	   public String getPassword() {
	      return password;
	   }

	   public void setPassword(String password) {
	      this.password = password;
	   }

	   protected long getId() {
	      return id;
	   }

	   protected void setId(long id) {
	      this.id = id;
	   }

	public static List list() {
	    SessionFactory sf = HibernateUtil.getSessionFactory();
	    Session session = sf.openSession();
	 
	    List user = session.createQuery("from User").list();
	    session.close();
	    return user;
	}

	public void setUserDetails(UserDetails userDetails) {
		this.userDetails = userDetails;
		
	}

	public UserDetails getUserDetails() {
		return userDetails;
	}
	
	public static void saveUser(User u, UserDetails ud) {
		
	    SessionFactory sf = HibernateUtil.getSessionFactory();
	    Session session = sf.openSession();
        session.beginTransaction();
	    u.setUserDetails(ud);
        ud.setUser(u);    
        session.save(ud);
        session.save(u);

        session.getTransaction().commit();
	    session.close();
	}
}
