package sample;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

public class sample {

	public static void main(String[] args) {

		generateDataBase();

		SessionFactory sf = HibernateUtil.getSessionFactory();
		Session session = sf.openSession();

		session.beginTransaction();

		// session.save(ud);
		// User.delete((long) 1);
		// System.out.println(u.getId());
		session.getTransaction().commit();

		System.out.println("******* READ *******");
		List users = User.list();
		System.out.println("Total Employees: " + users.size());

		session.close();

		// System.out.println(u.getName());
	}

	public static void generateDataBase() {

		User u = new User("Jan", "Haselko1");
		Picture p = new Picture("Fotka", "C://fotk.bmp");
		u.setPicture(p);
		UserDetails ud = new UserDetails("Marszałkowska", "Warszawa");
		User.saveUser(u, ud);

		u = new User("Magda", "Haselko2");
		ud = new UserDetails("Bukietowa", "Warszawa");
		p = new Picture("Profilowka", "C://fotk.bmp");
		u.setPicture(p);
		User.saveUser(u, ud);
		u = new User("Ola", "Haselko3");
		ud = new UserDetails("Zielona", "Warszawa");
		p = new Picture("Profilowka1", "C://fotk.bmp");
		u.setPicture(p);
		User.saveUser(u, ud);
		u = new User("Kamil", "Haselko4");
		ud = new UserDetails("Bukietowa", "Warszawa");
		p = new Picture("Profilowka", "C://fotk.bmp");
		u.setPicture(p);
		User.saveUser(u, ud);

		u = new User("Pawel", "Haselko5");
		ud = new UserDetails("Piękna", "Warszawa");
		p = new Picture("Profilowka", "C://fotk.bmp");
		u.setPicture(p);
		User.saveUser(u, ud);

		/*
		 * SessionFactory sf = HibernateUtil.getSessionFactory(); Session
		 * session = sf.openSession();
		 * 
		 * session.beginTransaction(); Post post = new Post();
		 * post.setSubject("Pierwszy post");
		 * post.setText("jedziemy z tym koksem!!! Dziwki!!!"); post.setUser(u);
		 * session.save(post); session.getTransaction().commit();
		 */
	}

}
