package sample;

public class Post {

	private Long PostId;
	private String subject ;
	private String text ;
	
	private User user_;

	public Long getPostId() {
		return PostId;
	}

	public void setPostId(Long postId) {
		PostId = postId;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public User getUser() {
		return user_;
	}

	public void setUser(User user) {
		this.user_ = user;
	}
	
	
}
